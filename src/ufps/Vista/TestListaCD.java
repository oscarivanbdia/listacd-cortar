/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class TestListaCD {
    
    public static void main(String[] args) {
        ListaCD<Persona> personas=new ListaCD();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
        System.out.println(personas);
        
        
        ListaCD<Persona> personas2=new ListaCD();
        personas2.insertarFin(new Persona(1,"madarme"));
        personas2.insertarFin(new Persona(3,"gederson"));
        personas2.insertarFin(new Persona(4,"diana"));
        System.out.println(personas2);
        /*
        Ejemplo: l1=<12,16,17,11,13,10> , l2=l1.cortar(2,4) 
             l1=<12,16,10> y l2=<17,11,13>
        */
        ListaCD<Integer> l1=new ListaCD();
        l1.insertarFin(12);
        l1.insertarFin(16);
        l1.insertarFin(17);
        l1.insertarFin(11);
        l1.insertarFin(13);
        l1.insertarFin(10);
        System.out.println(l1);
        System.out.println(l1.cortar(2, 2));
        System.out.println(l1);
    }
}
